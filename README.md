## Simple web crawler ##

Given a starting URL, collects links and builds a sitemap. Will only follow
links on the same domain. Continues either until it can't find any more links,
the specified timeout is reached or the user interrupts it.

Prints output (base links to child links found on the page) to a specified
file.

#### Usage ####

```
Usage of crawler:
  -max-inflight-requests int
        Maximum number of concurrent HTTP requests to allow (default 10)
  -out string
        File to save results to (default "/tmp/crawler-out")
  -queue-buffer int
        URLs to keep in the in memory buffer at any time (default 1000)
  -time-limit int
        How long to run the crawler for (default 30)
```

#### Building ####

You can either build locally using a system version of Go (I've used 1.8).
```
$ make
```
This outputs a binary to `bin/crawler`.


Alternatively you can build a Docker image with:
```
$ make build-docker
```

You can then run this:
```
$ docker run -v $(pwd):/out simple_crawler:latest /usr/local/bin/crawler -max-inflight-requests 45 -out /out/crawl-results -time-limit 10 https://en.wikipedia.org/
$ less crawl-results
```


#### Running tests ####

Again, tests can be run on the local system:
```
$ make test
```

or in a Docker container:
```
$ make test-docker
```

