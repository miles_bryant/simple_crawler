package main

import (
	"sync"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestStoreReadsandWritesSequentially(t *testing.T) {
	store := NewInMemorySiteMapStore()

	store.Put("http://test-host.dev", []string{"a", "b", "c", "d"})
	store.Put("http://test-host.dev/1", []string{"e"})
	store.Put("http://test-host.dev/2", []string{"f", "g"})

	assertSiteMap(t, store)
}

func TestStoreConsistentReadsandWritesConcurrently(t *testing.T) {
	for i := 0; i < 1000; i++ {
		doConcurrentReadAndWrite(t)
	}
}

func doConcurrentReadAndWrite(t *testing.T) {
	store := NewInMemorySiteMapStore()
	wg := &sync.WaitGroup{}

	wg.Add(3)
	concurrentPut := func(baseURL string, children ...string) {
		defer wg.Done()
		store.Put(baseURL, children)
		assert.True(t, store.Exists(baseURL))
		assert.Equal(t, store.FetchAll()[baseURL], children)
	}
	go concurrentPut("http://test-host.dev", "a", "b", "c", "d")
	go concurrentPut("http://test-host.dev/1", "e")
	go concurrentPut("http://test-host.dev/2", "f", "g")

	wg.Wait()
	assertSiteMap(t, store)

}

func assertSiteMap(t *testing.T, store *InMemorySiteMapStore) {
	assert.Equal(t, store.Count(), 3)
	assert.True(t, store.Exists("http://test-host.dev"))
	assert.True(t, store.Exists("http://test-host.dev/1"))
	assert.True(t, store.Exists("http://test-host.dev/2"))

	result := store.FetchAll()

	assert.Equal(t, []string{"a", "b", "c", "d"}, result["http://test-host.dev"])
	assert.Equal(t, []string{"e"}, result["http://test-host.dev/1"])
	assert.Equal(t, []string{"f", "g"}, result["http://test-host.dev/2"])
}
