package main

import (
	"sync"
)

// SiteMapStore stores a mapping of URLs to child pages
type SiteMapStore interface {
	// Exists returns true if a URL is present in the sitemap
	Exists(baseURL string) bool
	// Count returns the number of base URLs in the sitemap
	Count() int
	// Put adds a mapping between a base URL and its children
	Put(baseURL string, childLinks []string) error
}

// InMemorySiteMapStore stores the URL mapping entirely in memory. This is
// useful for testing purposes but is not safe for production use. This store
// is thread-safe and suitable to be used by multiple concurrent goroutines.
type InMemorySiteMapStore struct {
	store map[string][]string
	lock  *sync.Mutex
}

func NewInMemorySiteMapStore() *InMemorySiteMapStore {
	return &InMemorySiteMapStore{
		store: map[string][]string{},
		lock:  &sync.Mutex{},
	}
}

func (s InMemorySiteMapStore) Count() int {
	s.lock.Lock()
	defer s.lock.Unlock()
	return len(s.store)
}

func (s InMemorySiteMapStore) Exists(baseURL string) bool {
	s.lock.Lock()
	_, ok := s.store[baseURL]
	s.lock.Unlock()
	return ok
}
func (s *InMemorySiteMapStore) Put(baseURL string, childLinks []string) error {
	s.lock.Lock()
	s.store[baseURL] = childLinks
	s.lock.Unlock()
	return nil
}

// FetchAll returns a map of URLs to their links. This is an expensive
// operation as it copies the map backing the store. Any other operations will
// block until this operation completes.
func (s InMemorySiteMapStore) FetchAll() map[string][]string {
	storeCopy := map[string][]string{}
	s.lock.Lock()
	for base, children := range s.store {
		storeCopy[base] = children
	}
	s.lock.Unlock()
	return storeCopy
}
