package main

import "net/url"

type URLQueue interface {
	// Get returns a channel which receives the next URLs in the queue
	Get() <-chan *url.URL
	// Put appends a URL to the end of the queue.
	Put(URL *url.URL)
}

type InMemoryURLQueue struct {
	queue chan *url.URL
}

// NewInMemoryURLQueue creates a new queue, buffering the specified number of
// elements.
func NewInMemoryURLQueue() *InMemoryURLQueue {
	return &InMemoryURLQueue{
		queue: make(chan *url.URL),
	}
}

func (q *InMemoryURLQueue) Get() <-chan *url.URL {
	return q.queue
}

// Put appends a URL to the end of the queue in memory. This operation will
// block if the buffer fills up
func (q *InMemoryURLQueue) Put(URL *url.URL) {
	q.queue <- URL
}
