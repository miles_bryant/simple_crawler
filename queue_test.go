package main

import (
	"fmt"
	"net/url"
	"sync"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestInMemoryQueueReceivesFIFO(t *testing.T) {
	// this is probably overkill for simply testing a Golang channel, but I
	// wanted to check my assumptions about how they work
	queue := NewInMemoryURLQueue()

	wg := &sync.WaitGroup{}
	wg.Add(2)
	go func() {
		for i := 0; i < 5; i++ {
			u, _ := url.Parse(fmt.Sprintf("http://test-host.dev/%d", i))
			queue.Put(u)
		}
		wg.Done()
	}()
	go func() {
		for i := 0; i < 5; i++ {
			u, _ := <-queue.Get()
			expected, _ := url.Parse(fmt.Sprintf("http://test-host.dev/%d", i))
			assert.Equal(t, expected, u)
		}
		wg.Done()
	}()

	wg.Wait()
}
