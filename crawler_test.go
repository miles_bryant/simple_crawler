package main

import (
	"context"
	"fmt"
	"net/http"
	"net/url"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	gock "gopkg.in/h2non/gock.v1"
)

func TestCrawlerCreatesSimpleSiteMapAndIgnoresExternalLinks(t *testing.T) {
	sitemap := map[string][]string{
		"http://test-host.dev/":  []string{"http://test-host.dev/a", "http://test-host.dev/b"},
		"http://test-host.dev/a": []string{"http://test-host.dev/", "http://external-link.dev/foo"},
		"http://test-host.dev/b": []string{"http://test-host.dev/a"},
	}
	doCrawlerE2ETest(t, sitemap, sitemap, 1)
}

func doCrawlerE2ETest(t *testing.T, sitemap map[string][]string, expected map[string][]string, maxInflightReqs int) {
	defer gock.Off()

	startURL, _ := url.Parse("http://test-host.dev/")
	inMemoryQueue := NewInMemoryURLQueue()
	store := NewInMemorySiteMapStore()

	client := &http.Client{
		Timeout: 5 * time.Second,
	}
	gock.InterceptClient(client)
	crawler := NewCrawler(startURL, inMemoryQueue, store, client, maxInflightReqs)

	setupHTTPExpectations(sitemap)

	timeoutCtx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	err := crawler.Crawl(timeoutCtx)
	require.Nil(t, err)

	assert.Equal(t, expected, store.FetchAll())
	unmatchedReqs := gock.GetUnmatchedRequests()
	if len(unmatchedReqs) > 0 {
		var unmatchedReqsStr string
		for _, m := range unmatchedReqs {
			unmatchedReqsStr += fmt.Sprintf("%s,", m.URL.String())
		}
		assert.Fail(t, fmt.Sprintf("There were %d unmatched requests: %s", len(unmatchedReqs), unmatchedReqsStr))
	}
	cancel()
}

func setupHTTPExpectations(sitemap map[string][]string) {
	for path, links := range sitemap {
		gock.New(path).Get("/").Reply(200).BodyString(generateHTML(links))
	}
}

func generateHTML(links []string) string {
	var linksHTML string
	for _, l := range links {
		linksHTML += fmt.Sprintf(`<a href="%s">Link</a>`, l) + "\n"
	}
	return fmt.Sprintf("<html>\n%s</html>", linksHTML)
}
