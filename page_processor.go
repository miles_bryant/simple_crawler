package main

import (
	"io"
	"log"

	"net/url"

	"golang.org/x/net/html"
)

// ExtractLinksFromHTML reads a HTML document and extracts links from <a> tags,
// returning a list of URLs. It resolves relative links using the document's
// URL.
func ExtractLinksFromHTML(pageURL *url.URL, body io.Reader) ([]*url.URL, error) {
	urls := []*url.URL{}

	t := html.NewTokenizer(body)
	for {
		nextT := t.Next()
		switch nextT {
		case html.ErrorToken:
			return urls, nil
		case html.StartTagToken:
			token := t.Token()
			if token.Data == "a" {
				for _, a := range token.Attr {
					if a.Key == "href" {
						if a.Val == "" || a.Val == "#" {
							continue
						}
						hrefURL, err := url.Parse(a.Val)
						if err != nil {
							log.Printf("Error normalising URL: %s", err.Error())
							continue
						}
						normalised := pageURL.ResolveReference(hrefURL)
						if normalised.Path == "" {
							normalised.Path = "/"
						}
						if isHTTPProtocol(normalised) {
							urls = append(urls, normalised)
						}
					}
				}
			}
		}
	}
	return urls, nil
}

func isHTTPProtocol(url *url.URL) bool {
	return url.Scheme == "http" || url.Scheme == "https"
}
