FROM golang:1.8-alpine AS builder

RUN apk add --no-cache --update git

ADD . /go/src/bitbucket.org/miles_bryant
WORKDIR /go/src/bitbucket.org/miles_bryant

RUN go get ./...

RUN mkdir /out
RUN CGO_ENABLED=0 GOOS=linux go build -a -o /out/crawler .

FROM alpine:3.6

RUN apk add --no-cache --update ca-certificates
COPY --from=builder /out/crawler /usr/local/bin/crawler

CMD ["/usr/local/bin/crawler"]

