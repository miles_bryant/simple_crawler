.DEFAULT_GOAL := build
build:
	mkdir -p bin/
	go get -u ./...
	go build -o bin/crawler .

test:
	go get -u ./...
	# Need some proper dependency management here :/
	go get -u github.com/stretchr/testify/assert/... gopkg.in/h2non/gock.v1/...
	go test ./... -v

build-docker:
	docker build -t simple_crawler:latest .

test-docker:
	docker build -t simple_crawler_tests:latest --file=Dockerfile-test .
	docker run --rm --name=simple_crawler_tests simple_crawler_tests:latest

