package main

import (
	"bytes"
	"net/url"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestExtractsLinksFromHTMLDocument(t *testing.T) {
	docURL, _ := url.Parse("http://test-host.dev/parentpage")

	body := `<html>
	<a href="/a">A</a>
	<a href="http://test-host.dev/b" target="_blank">A</a>
	<a href="//test-host.dev/c/1">A</a>
	<a href="http://other-host.com">A</a>
	<a href="d/1/2/3">A</a>
	<a href="https://test-host.dev/e" target="_blank">A</a>
	</html>`

	urls, err := ExtractLinksFromHTML(docURL, strings.NewReader(body))
	assert.Nil(t, err, "Expected no error")

	require.Len(t, urls, 6, "Expected 6 links to be harvested")

	assertURLEq(t, "http://test-host.dev/a", urls[0])
	assertURLEq(t, "http://test-host.dev/b", urls[1])
	assertURLEq(t, "http://test-host.dev/c/1", urls[2])
	assertURLEq(t, "http://other-host.com/", urls[3])
	assertURLEq(t, "http://test-host.dev/d/1/2/3", urls[4])
	assertURLEq(t, "https://test-host.dev/e", urls[5])
}

func TestIgnoresNonHTTPLinks(t *testing.T) {
	docURL, _ := url.Parse("http://test-host.dev/parentpage")

	body := `<html>
	<a href="file:///test-host.dev/foo">A</a>
	<a href="javascript:alert('Hello')">A</a>
	<a href="tel://0123456789">A</a>
	</html>`
	urls, err := ExtractLinksFromHTML(docURL, strings.NewReader(body))
	assert.Nil(t, err, "Expected no error")

	assert.Empty(t, urls, "Expected all non-http(s) links to be ignored")
}

func TestGracefullyHandlesInvalidHTML(t *testing.T) {
	docURL, _ := url.Parse("http://test-host.dev/parentpage")

	body := []byte{45, 11, 163, 72, 12, 244, 77}
	urls, err := ExtractLinksFromHTML(docURL, bytes.NewReader(body))
	assert.Nil(t, err, "Expected no error")

	assert.Empty(t, urls, "Expected all non-http(s) links to be ignored")
}
func assertURLEq(t *testing.T, expURLstr string, actualURL *url.URL) {
	expURL, _ := url.Parse(expURLstr)
	assert.Equal(t, expURL, actualURL)
}
