package main

import (
	"context"
	"log"
	"net/http"
	"net/url"
)

// Crawler crawls a single site given a starting page, storing results in a
// SiteMapStore.
type Crawler struct {
	URLQueue
	SiteMapStore
	*http.Client
	StartURL *url.URL
	sem      *Semaphore
}

func NewCrawler(startURL *url.URL, queue URLQueue, store SiteMapStore, HTTPClient *http.Client, maxInflightReqs int) *Crawler {
	return &Crawler{
		URLQueue:     queue,
		SiteMapStore: store,
		Client:       HTTPClient,
		StartURL:     startURL,
		sem:          NewSemaphore(maxInflightReqs),
	}
}

type empty struct{}

type Semaphore struct {
	sem chan empty
}

func NewSemaphore(capacity int) *Semaphore {
	return &Semaphore{
		sem: make(chan empty, capacity),
	}
}

func (s *Semaphore) Acquire() {
	s.sem <- empty{}
}

func (s *Semaphore) Count() int {
	return len(s.sem)
}

func (s *Semaphore) Release() {
	<-s.sem
}

func (s *Semaphore) Empty() {
	for i := 0; i < cap(s.sem); i++ {
		s.sem <- empty{}
	}
}

// Crawl starts crawling a single site, given a startURL.
func (c *Crawler) Crawl(ctx context.Context) error {
	go c.URLQueue.Put(c.StartURL)

	for {
		select {
		case <-ctx.Done():
			log.Printf("context cancelled")
			return nil
		case nextURL := <-c.URLQueue.Get():
			if !c.SiteMapStore.Exists(nextURL.String()) {
				go func() {
					c.sem.Acquire()
					defer c.sem.Release()
					c.crawlURL(nextURL)
				}()
			}
		default:
			if c.SiteMapStore.Count() > 0 && c.sem.Count() == 0 {
				log.Printf("Finished processing all URLs in queue")
				return nil
			}
		}
	}
}

func (c *Crawler) crawlURL(u *url.URL) error {
	resp, err := c.Client.Get(u.String())
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	links, err := ExtractLinksFromHTML(u, resp.Body)
	if err != nil {
		return err
	}
	storedLinks := make([]string, len(links))
	for i, l := range links {
		if !c.SiteMapStore.Exists(l.String()) && l.Host == c.StartURL.Host {
			c.URLQueue.Put(l)
		}
		storedLinks[i] = l.String()
	}

	c.SiteMapStore.Put(u.String(), storedLinks)
	return nil
}
