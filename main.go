package main

import (
	"context"
	"flag"
	"fmt"
	"net/http"
	"net/url"
	"os"
	"os/signal"
	"strings"
	"time"
)

var (
	timeLimitSeconds int
	maxInflightReqs  int
	queueBuffer      int
	outputFile       string
)

func parseStartURL() (*url.URL, error) {
	startURLRaw := flag.Arg(0)
	if startURLRaw == "" {
		return &url.URL{}, fmt.Errorf("expected one argument with base URL")
	}

	if !(strings.HasPrefix(startURLRaw, "http://") || strings.HasPrefix(startURLRaw, "https://")) {
		startURLRaw = "http://" + startURLRaw
	}

	u, err := url.Parse(startURLRaw)
	if err != nil {
		return &url.URL{}, err
	}

	if u.Path == "" {
		u.Path = "/"
	}
	return u, err
}

func exitWithUsage(err error) {
	fmt.Printf("Error: %s\n", err.Error())
	fmt.Printf("Usage of %s:\n", os.Args[0])
	flag.PrintDefaults()
	os.Exit(1)
}

func main() {
	flag.IntVar(&timeLimitSeconds, "time-limit", 30, "How long to run the crawler for")
	flag.IntVar(&maxInflightReqs, "max-inflight-requests", 10, "Maximum number of concurrent HTTP requests to allow")
	flag.IntVar(&queueBuffer, "queue-buffer", 1000, "URLs to keep in the in memory buffer at any time")
	flag.StringVar(&outputFile, "out", "/tmp/crawler-out", "File to save results to")
	flag.Parse()

	outF, err := os.Create(outputFile)
	if err != nil {
		exitWithUsage(fmt.Errorf("Could not create file for output: %s", err.Error()))
	}

	startURL, err := parseStartURL()
	if err != nil {
		exitWithUsage(err)
	}
	if timeLimitSeconds <= 0 {
		exitWithUsage(fmt.Errorf("--time-limit must be greater than zero"))
	}
	if maxInflightReqs <= 0 {
		exitWithUsage(fmt.Errorf("--max-inflight-requests must be greater than zero"))
	}
	if queueBuffer <= 0 {
		exitWithUsage(fmt.Errorf("--queue-buffer must be greater than zero"))
	}

	timeLimit := time.Duration(timeLimitSeconds)

	client := &http.Client{
		Timeout: 5 * time.Second,
	}
	inMemoryQueue := NewInMemoryURLQueue()

	store := NewInMemorySiteMapStore()
	crawler := NewCrawler(startURL, inMemoryQueue, store, client, maxInflightReqs)

	fmt.Printf("Starting crawl on URL %s, crawling for up to %ds with maximum of %d requests in flight\n", startURL, timeLimitSeconds, maxInflightReqs)
	context, cancel := context.WithTimeout(context.Background(), timeLimit*time.Second)

	interrupts := make(chan os.Signal, 2)
	signal.Notify(interrupts, os.Interrupt)
	go func() {
		<-interrupts
		cancel()
	}()

	crawler.Crawl(context)

	sitemap := store.FetchAll()

	fmt.Printf("%d entries in sitemap\n", len(sitemap))
	fmt.Printf("Saving sitemap to %s\n", outputFile)

	startURLstr := startURL.String()

	for base, children := range sitemap {
		var c string
		lenC := len(children)
		for i, l := range children {
			c += "/" + strings.TrimPrefix(l, startURLstr)
			if i != lenC-1 {
				c += "\n\t"
			}
		}
		fmt.Fprintf(outF, "%s:\n\t%s", base, c)
	}
}
